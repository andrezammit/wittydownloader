﻿using System;

namespace WittyDownloader
{
    public static class Constants
    {
        public static class Links
        {
            public static class YoutubeDl
            {
                public const String Other = "https://yt-dl.org/downloads/latest/youtube-dl";
                public const String Windows = "https://yt-dl.org/downloads/latest/youtube-dl.exe";
            }
            
            public const String VideoDataSource = "https://feed.entertainment.tv.theplatform.eu/f/PR1GhC/mediaset-prod-ext-programs/guid/-/";
            public const String VideoDataSourceByGuid = "https://feed.entertainment.tv.theplatform.eu/f/PR1GhC/mediaset-prod-all-programs?byGuid=";
        }

        public static class XPaths
        {
            public const String VideoIframe = "//*[@id='playeriframe']";
            public const String VideoElement_1 = "//*[@id='vjs_video_1_html5_api']";
            public const String VideoElement_3 = "//*[@id='vjs_video_3_html5_api']";
        }

        public static class FileNames
        {
            public static class YoutubeDl
            {
                public const String Other = "youtube-dl";
                public const String Windows = "youtube-dl.exe";
            }

            public const String TempDir = "WittyDownloader";
        }
    }
}
