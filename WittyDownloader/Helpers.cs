﻿using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace WittyDownloader
{
    public static class OperatingSystem
    {
        public static bool IsWindows() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        public static bool IsMacOS() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

        public static bool IsLinux() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
    }

    public static class Helpers
    {
        public static string TempDir { get; private set; }
        public static string BinaryDir { get; private set; }

        public static HttpClient HttpClient { get; } = new();

        static Helpers()
        {
            TempDir = GetTempDir();
            BinaryDir = GetBinaryDir();

            CreateTempDir();
        }

        public static string GetYoutubeDlFileName()
        {
            if (OperatingSystem.IsWindows())
            {
                return Constants.FileNames.YoutubeDl.Windows;
            }

            return Constants.FileNames.YoutubeDl.Other;
        }

        public static string GetYoutubeDlUrl()
        {
            if (OperatingSystem.IsWindows())
            {
                return Constants.Links.YoutubeDl.Windows;
            }

            return Constants.Links.YoutubeDl.Other;
        }

        public static bool IsYoutubeDlAvailable()
        {
            return File.Exists(GetYoutubeDlPath());
        }

        public static string GetYoutubeDlPath()
        {
            return Path.Combine(Helpers.TempDir, GetYoutubeDlFileName());
        }

        public static async Task DownloadYoutubleDl()
        {
            var youtubeDlUrl = GetYoutubeDlUrl();

            var response = await HttpClient.GetAsync(
                youtubeDlUrl,
                HttpCompletionOption.ResponseHeadersRead);

            response.EnsureSuccessStatusCode();

            var content = response.Content;
            var contentStream = await content.ReadAsStreamAsync();

            using (var fileStream = File.Create(GetYoutubeDlPath()))
            {
                await contentStream.CopyToAsync(fileStream);
            }
        }

        public static string GetFileNameFromUrl(string url)
        {
            url = url.Trim('/');

            var lastSlash = url.LastIndexOf('/');
            var fileName = url.Substring(lastSlash + 1);

            return fileName;
        }

        public static string GetYoutubeDlOutputPath(string downloadPath, string fileName)
        {
            if (downloadPath.Length == 0)
            {
                downloadPath = BinaryDir;
            }

            fileName += ".mp4";
            return Path.Combine(downloadPath, fileName);
        }

        private static string GetBinaryDir()
        {
            var binaryPath = Assembly.GetExecutingAssembly().Location;

            var binaryDir = Path.GetDirectoryName(binaryPath);

            if (binaryDir == null)
            {
                throw new NullReferenceException($"Failed to get current directory path.");
            }

            return binaryDir;
        }

        public static string GetTempDir()
        {
            return Path.Combine(Path.GetTempPath(), Constants.FileNames.TempDir);
        }

        public static void CreateTempDir()
        {
            Directory.CreateDirectory(TempDir);
        }
    }
}
