﻿using System;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using NYoutubeDL;

namespace WittyDownloader
{
    public class Engine
    {
        private string _downloadPath = "";

        private ChromeDriver StartChromeDriver()
        {
            Console.WriteLine("Starting Chrome driver...");

            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            
            var chromeDriverDirectory = Helpers.BinaryDir;

            var service = ChromeDriverService.CreateDefaultService(chromeDriverDirectory);
            service.SuppressInitialDiagnosticInformation = true;
            
            var chromeDriver = new ChromeDriver(chromeDriverDirectory, chromeOptions);
            chromeDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            return chromeDriver;
        }

        public void SetDownloadPath(string path)
        {
            _downloadPath = path;
        }

        private string? GetGuidFromUrl(string url)
        {
            var index = url.LastIndexOf("_");

            if (index == -1)
            {
                return null;
            }

            return url.Substring(index + 1);
        }

        public async Task DownloadVideoFromUrl(string url)
        {
            Console.WriteLine($"Starting process to download video from {url}...");
            
            var guid = GetGuidFromUrl(url);

            string videoMetaDataSource;

            if (!string.IsNullOrEmpty(guid))
            {
                videoMetaDataSource = GetVideoDataSourceByGuid(guid);
            }
            else
            {
                string videoId;

                using (var chromeDriver = StartChromeDriver())
                {
                    chromeDriver.Navigate().GoToUrl(url);

                    try
                    {
                        var frameElement = GetIFrameElement(chromeDriver);

                        var playerSrc = GetPlayerIframeSrc(frameElement);
                        chromeDriver.Navigate().GoToUrl(playerSrc);
                    }
                    catch (NoSuchElementException)
                    {
                        Console.WriteLine("iFrame not found, searching in same page...");
                    }

                    var videoElement = GetVideoElement(chromeDriver);
                    videoId = await GetVideoId(videoElement);

                    chromeDriver.Close();
                }

                videoMetaDataSource = GetVideoDataSource(videoId);
            }

            var publicUrl = await GetPublicUrl(videoMetaDataSource);

            Console.WriteLine($"Public URL is {publicUrl}.");

            var fileName = Helpers.GetFileNameFromUrl(url);
            await DownloadVideo(fileName, publicUrl, _downloadPath);
        }

        private IWebElement GetIFrameElement(ChromeDriver chromeDriver)
        {
            return chromeDriver.FindElementByXPath(Constants.XPaths.VideoIframe);
        }

        private IWebElement GetVideoElement(ChromeDriver chromeDriver)
        {
            try
            {
                return chromeDriver.FindElementByXPath(Constants.XPaths.VideoElement_3);
            }
            catch (NoSuchElementException)
            {
                return chromeDriver.FindElementByXPath(Constants.XPaths.VideoElement_1);
            }
        }

        private string GetPlayerIframeSrc(IWebElement frameElement)
        {
            return frameElement.GetAttribute("src");
        }

        private async Task<string> GetVideoId(IWebElement videoElement)
        {
            string posterUrl;

            bool isValidPosterUrl;

            do
            {
                posterUrl = videoElement.GetAttribute("poster");

                isValidPosterUrl = posterUrl.Length > 0;

                if (!isValidPosterUrl)
                {
                    await Task.Delay(1000);
                }
            }
            while (!isValidPosterUrl);

            var lastSlash = posterUrl.LastIndexOf('/') + 1;
            posterUrl = posterUrl.Remove(0, lastSlash);

            var firstDash = posterUrl.IndexOf('-');
            posterUrl = posterUrl.Remove(firstDash);

            return posterUrl;
        }

        private string GetVideoDataSourceByGuid(string guid)
        {
            return $"{Constants.Links.VideoDataSourceByGuid}{guid}";
        }

        private string GetVideoDataSource(string videoId)
        {
            return Constants.Links.VideoDataSource + videoId;
        }

        private async Task<string> GetPublicUrl(string videoMetaDataSource)
        {
            var response = await Helpers.HttpClient.GetStringAsync(videoMetaDataSource);
            var jsonData = JObject.Parse(response);

            var mediaObj = jsonData["media"];

            if (mediaObj == null)
            {
                var entriesObj = jsonData["entries"][0];
                mediaObj = entriesObj["media"];
            }

            var publicUrl = mediaObj[0]["publicUrl"];

            return publicUrl.ToString();
        }

        private async Task DownloadVideo(string fileName, string publicUrl, string downloadPath)
        {
            if (!Helpers.IsYoutubeDlAvailable())
            {
                Console.WriteLine("Youtube-dl is not available. Downloading now...");
                await Helpers.DownloadYoutubleDl();
            }

            var videoOutputPath = Helpers.GetYoutubeDlOutputPath(downloadPath, fileName);

            var youtubeDl = new YoutubeDL(Helpers.GetYoutubeDlPath());

            youtubeDl.Options.GeneralOptions.Update = true;

            youtubeDl.Options.FilesystemOptions.CacheDir = Helpers.TempDir;
            youtubeDl.Options.FilesystemOptions.Output = videoOutputPath;

            youtubeDl.StandardOutputEvent += (sender, output) => Console.WriteLine(output);
            youtubeDl.StandardErrorEvent += (sender, errorOutput) => Console.WriteLine(errorOutput);

            await youtubeDl.DownloadAsync(publicUrl);

            Console.WriteLine("Video downloaded to {0}.", videoOutputPath);
        }
    }
}
