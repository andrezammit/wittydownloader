﻿using System;
using System.Threading.Tasks;

namespace WittyDownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            try
            {
                if (!ValidateArgs(args))
                {
                    return;
                }

                var engine = new Engine();

                if (args.Length > 1)
                {
                    engine.SetDownloadPath(args[1]);
                }

                await engine.DownloadVideoFromUrl(args[0]);

                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static bool ValidateArgs(string[] args)
        {
            if (args.Length == 0)
            {
                throw new ArgumentException("Please enter a Witty TV video URL as a parameter.");
            }

            return true;
        }
    }
}
